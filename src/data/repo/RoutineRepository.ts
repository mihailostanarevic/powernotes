import Routine, { ROUTINES_KEY_NAME } from '../model/Routine';
import { BaseDataRepository } from './BaseDataRepository';

export class RoutineRepository extends BaseDataRepository<Routine> {
  protected override collectionKey: string = ROUTINES_KEY_NAME;
}
