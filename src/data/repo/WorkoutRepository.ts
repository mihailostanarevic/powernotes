import Workout, { WORKOUTS_KEY_NAME } from '../model/Workout';
import { BaseDataRepository } from './BaseDataRepository';

export class WorkoutRepository extends BaseDataRepository<Workout> {
  protected override collectionKey: string = WORKOUTS_KEY_NAME;
}
