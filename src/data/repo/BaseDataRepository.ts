import uuid from 'react-native-uuid';
import { storeData, getData } from '../util/AsyncStorageHandler';

export abstract class BaseDataRepository<T> {
  protected collectionKey: string = '';

  async create(payload: T): Promise<T> {
    const collection: any[] = await getData(this.collectionKey);
    collection.push({ id: uuid.v4() as string, ...payload });
    return storeData(this.collectionKey, collection) as T;
  }
  async update(id: string, payload: any): Promise<T> {
    const collection: any[] = await getData(this.collectionKey);
    const index = collection.findIndex((item) => item.id === id);
    collection[index] = { ...payload };
    return storeData(this.collectionKey, collection) as T;
  }
  async delete(id: string) {
    const collection: any[] = await getData(this.collectionKey);
    storeData(
      this.collectionKey,
      collection.filter((item) => item.id !== id)
    );
  }
  async getAll(): Promise<T[]> {
    const res = await getData(this.collectionKey);
    return res.map((item: any) => item as T);
  }
  async getById(id: string): Promise<T> {
    const collection: any[] = await getData(this.collectionKey);
    return collection.find((item) => item.id === id);
  }
}
