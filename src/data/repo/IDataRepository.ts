interface IDataRepository {
  create(collectionName: string, payload: any): Promise<any>;
  update(collectionName: string, id: string, payload: any): Promise<any>;
  delete(collectionName: string, id: string): void;
  getAll(collectionName: string): Promise<any[]>;
  getById(collectionName: string, id: string): Promise<any>;
}
