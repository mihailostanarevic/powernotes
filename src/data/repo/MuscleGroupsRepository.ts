import { MUSCLE_GROUPS_KEY_NAME, MuscleGroup } from '../model/MuscleGroup';
import { BaseDataRepository } from './BaseDataRepository';

export class MuscleGroupsRepository extends BaseDataRepository<MuscleGroup> {
  protected override collectionKey: string = MUSCLE_GROUPS_KEY_NAME;
}
