import Exercise, { EXERCISES_KEY_NAME } from '../model/Exercise';
import { BaseDataRepository } from './BaseDataRepository';

export class ExerciseRepository extends BaseDataRepository<Exercise> {
  protected override collectionKey: string = EXERCISES_KEY_NAME;
}
