import { Set } from './Set';

export default interface Workout {
  id: number;
  name: string;
  note?: string;
  started: Date;
  finished?: Date;
  sets: Set[];
}

export const WORKOUTS_KEY_NAME = 'workouts';
