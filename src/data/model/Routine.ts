import Exercise from './Exercise';
import { ExerciseSet } from './ExerciseSet';

export default interface Routine {
  id: number;
  name: string;
  exercises: ExerciseSet[];
}

export const ROUTINES_KEY_NAME = 'routines';
