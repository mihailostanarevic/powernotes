export interface MuscleGroup {
  id: number;
  name: string;
  description?: string;
}

export const MUSCLE_GROUPS_KEY_NAME = 'muscle_groups';
