export interface Set {
  id: number;
  metric: string;
  weight: number;
  reps: number;
  rpe?: number;
  note?: string;
}
