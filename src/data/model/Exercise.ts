import { MuscleGroup } from './MuscleGroup';

export default interface Exercise {
  id: string;
  name: string;
  unilateral: boolean;
  description: string;
  muscleGroupsActivated: MuscleGroup[];
}

export const EXERCISES_KEY_NAME: string = 'exercises';
