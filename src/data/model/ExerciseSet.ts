import { Set } from './Set';

export interface ExerciseSet {
  id: string;
  name: string;
  sets: Set[];
}
