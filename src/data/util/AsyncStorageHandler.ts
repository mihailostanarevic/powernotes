import AsyncStorage from '@react-native-async-storage/async-storage';

export async function storeData(key: string, value: any): Promise<any> {
  try {
    const jsonValue = JSON.stringify(value);
    await AsyncStorage.setItem(key, jsonValue);
    return jsonValue;
  } catch (e) {
    console.error(e);
  }
}

export async function getData(key: string) {
  try {
    const res = await AsyncStorage.getItem(key);
    return res === null ? [] : JSON.parse(res);
  } catch (e) {
    console.error(e);
  }
}
