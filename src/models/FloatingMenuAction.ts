export interface FloatingMenuAction {
	icon: any;
	onPress: () => void;
}
