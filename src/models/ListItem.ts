export interface ListItem<T> {
	id: number | string;
	data: T;
	name: string;
	selected: boolean;
}
