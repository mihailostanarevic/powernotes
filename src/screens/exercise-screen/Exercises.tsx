import React, { useState } from 'react';
import { Pressable, StyleSheet, TextInput, View } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import Fonts from '../../utill/Fonts';
import Colors from '../../utill/Colors';
import List from '../../components/List';
import MultipleCheckList from '../../components/MultipleCheckList';
import { ListItem } from '../../models/ListItem';
import { FloatingMenuAction } from '../../models/FloatingMenuAction';
import FloatingMenu from '../../components/FloatingMenu';

const muscleGroups = ['Chest', 'Shoulders', 'Back', 'Biceps', 'Legs'];

const menuIcons: FloatingMenuAction[] = [
  {
    icon: 'pencil',
    onPress: () => {
      console.log('edit');
    }
  },
  {
    icon: 'timer',
    onPress: () => {
      console.log('history');
    }
  },
  {
    icon: 'trash',
    onPress: () => {
      console.log('trash');
    }
  }
];
const exercisesList: ListItem<any>[] = [
  {
    id: 0,
    name: 'Bench Press',
    selected: false,
    data: 'Bench Press'
  },
  {
    id: 1,
    name: 'Dumbell Fly',
    selected: false,
    data: 'Dumbell Fly'
  },
  {
    id: 2,
    name: 'Deadlift',
    selected: false,
    data: 'Deadlift'
  },
  {
    id: 3,
    name: 'Barbell Squat',
    selected: false,
    data: 'Barbell Squat'
  }
];

const Exercises: React.FC = () => {
  const [showFitlers, setShowFilters] = useState(false);
  const [exercises, setExercises] = useState(exercisesList);

  const toggleFilter = () => {
    setShowFilters((filterVisible) => !filterVisible);
  };

  const filterApplied = (value: string) => {
    const upperCaseValue = value.toUpperCase();
    setExercises(() =>
      value.length > 0
        ? exercisesList.filter((item) =>
            item.name.toUpperCase().includes(upperCaseValue)
          )
        : exercisesList
    );
  };

  const isItemSelected = (): boolean => {
    return exercises.filter((e) => e.selected)?.length > 0;
  };

  const onSelect = (exerciseSelected: any) => {
    const e = [...exercises].map((element) => {
      return {
        ...element,
        selected: element.id !== exerciseSelected.id ? false : element.selected
      };
    });
    const index = e.findIndex((element) => element.id === exerciseSelected.id);
    console.log(index);
    if (index >= 0) {
      e[index] = {
        ...e[index],
        selected: !e[index].selected
      };

      setExercises(e);
    } else {
      setExercises([exerciseSelected]);
    }
  };

  return (
    <>
      <View style={styles.mainContainer}>
        <View style={styles.headerContainer}>
          <View style={styles.searchContainer}>
            <Ionicons name="search" style={styles.searchIcon} size={20} />
            <TextInput
              style={styles.searchInput}
              placeholder="Search"
              onChangeText={filterApplied}
            ></TextInput>
          </View>
          <View style={styles.menuContainer}>
            <Ionicons style={styles.menuIcon} name="add" size={32} />
            <View>
              <Pressable onPress={toggleFilter}>
                <Ionicons style={styles.menuIcon} name="filter" size={32} />
              </Pressable>
              {showFitlers && <MultipleCheckList items={muscleGroups} />}
            </View>
          </View>
        </View>
        <List items={exercises} itemSelected={onSelect} />
      </View>
      {isItemSelected() && <FloatingMenu actions={menuIcons} />}
    </>
  );
};

export default Exercises;

const styles = StyleSheet.create({
  mainContainer: {
    padding: 8
  },
  headerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  searchContainer: {
    width: '70%',
    alignItems: 'center',
    flexDirection: 'row'
  },
  searchInput: {
    fontFamily: Fonts.medium500,
    borderBottomWidth: 0.5,
    fontSize: 16,
    width: '100%',
    paddingHorizontal: 2,
    marginLeft: 4
  },
  menuContainer: {
    flexDirection: 'row'
  },
  searchIcon: {
    color: Colors.secondary700
  },
  menuIcon: {
    marginRight: 8,
    color: Colors.primary500
  }
});
