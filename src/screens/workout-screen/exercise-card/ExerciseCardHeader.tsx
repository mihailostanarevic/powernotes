import React from 'react';
import { View, Text, StyleSheet, Pressable } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import Colors from '../../../utill/Colors';

type Props = {
  title: string;
  addRowHandler: () => void;
};

const ExerciseCardHeader: React.FC<Props> = ({ title, addRowHandler }) => {
  return (
    <View style={style.exerciseHeader}>
      <Text style={style.title}>{title}</Text>
      <Pressable onPress={addRowHandler} style={style.addButton}>
        <Ionicons name="add-outline" size={38} color={Colors.primary500} />
      </Pressable>
    </View>
  );
};

export default ExerciseCardHeader;

const style = StyleSheet.create({
  exerciseHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    verticalAlign: 'center'
  },
  title: {
    textAlign: 'center',
    fontSize: 20,
    fontFamily: 'Poppins_700Bold',
    paddingTop: 7,
    marginBottom: -10
  },
  addButton: {
    right: 0,
    position: 'absolute'
  }
});
