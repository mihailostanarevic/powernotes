import React from 'react';
import { View, StyleSheet } from 'react-native';
import { Table, Row } from 'react-native-table-component';

type Props = {
  data: any;
};

const ExerciseCardBody: React.FC<Props> = ({ data }) => {
  const columns: string[] = ['Set', 'Weight', 'Reps', 'RPE', ''];

  return (
    <View style={styles.exerciseCard}>
      <Table>
        <Row data={columns} style={styles.row} textStyle={styles.columnText} />
        {data.map((rowData: any, index: number) => (
          <Row data={rowData} key={index} style={styles.row} />
        ))}
      </Table>
    </View>
  );
};

export default ExerciseCardBody;

const styles = StyleSheet.create({
  exerciseCard: {
    paddingTop: 25
  },
  row: {
    height: 35
  },
  columnText: {
    textAlign: 'center',
    fontWeight: 'bold'
  }
});
