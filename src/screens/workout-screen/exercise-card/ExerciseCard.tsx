import React, { useEffect, useState } from 'react';
import ExerciseCardBody from './ExerciseCardBody';
import ExerciseCardHeader from './ExerciseCardHeader';
import Card from '../../../components/Card';
import Cell from '../../../components/Cell';
import { StyleSheet, View, Pressable } from 'react-native';
import Colors from '../../../utill/Colors';
import Ionicons from '@expo/vector-icons/Ionicons';

type Props = {
  exercise: any;
};

const ExerciseCard: React.FC<Props> = ({ exercise }) => {
  const dummyReps: number[] = [10, 8, 7, 4];

  const [tableRows, setTableRows] = useState<any>([]);

  useEffect(() => {
    setTableRows(
      exercise.weights.map((weight: number, index: number) =>
        createRow(weight, index)
      )
    );
  }, [setTableRows]);

  const updateCellValue = (
    rowIndex: number,
    colIndex: number,
    newValue: number
  ) => {
    console.log(rowIndex, colIndex, newValue);
    // setTableData((prev) => {
    //    const newData = [...prev];
    //    newData[rowIndex][colIndex] = newValue;
    //    return newData;
    // });
  };

  const handleAddSetClicked = () => {
    setTableRows((prev: any) => {
      const newData = [...prev];
      newData[newData.length] = createRow(0, newData.length);
      return newData;
    });
  };

  const createRow = (weight: number, index: number) => {
    return [
      <Cell
        value={(index + 1).toString()}
        editable={false}
        style={styles.nonEditableText}
        placeholder={'0'}
      ></Cell>,
      <Cell
        value={weight.toString()}
        type="numeric"
        updateFunction={(val: number) => updateCellValue(index, 1, val)}
        editable={true}
        style={styles.editableText}
        placeholder={'0'}
      ></Cell>,
      <Cell
        value={(dummyReps[index] ?? 0).toString()}
        type="numeric"
        updateFunction={(val: number) => updateCellValue(index, 2, val)}
        editable={true}
        style={styles.editableText}
        placeholder={'0'}
      ></Cell>,
      <Cell
        value={undefined}
        type="numeric"
        updateFunction={(val: number) => updateCellValue(index, 3, val)}
        editable={true}
        style={styles.editableText}
        placeholder={'0'}
      ></Cell>,
      <View style={styles.actionButtons}>
        <Pressable onPress={removeRow.bind(this, index)}>
          <View>
            <Ionicons
              name="trash-outline"
              size={24}
              color={Colors.primary500}
            />
          </View>
        </Pressable>
        <Pressable>
          <View>
            <Ionicons
              name="create-outline"
              size={24}
              color={Colors.primary500}
            />
          </View>
        </Pressable>
      </View>
    ];
  };

  const removeRow = (rowIndex: number) => {
    setTableRows((prev: any) => {
      const newData = [...prev];
      newData.splice(rowIndex, 1);
      return newData;
    });
  };

  return (
    <Card>
      <ExerciseCardHeader
        addRowHandler={handleAddSetClicked}
        title={exercise.name}
      />
      <ExerciseCardBody data={tableRows} />
    </Card>
  );
};

export default ExerciseCard;

const styles = StyleSheet.create({
  actionButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    verticalAlign: 'middle',
    flex: 1,
    paddingTop: 5
  },
  editableText: {
    textAlign: 'center',
    color: '#000000',
    fontFamily: 'Poppins_700Bold'
  },
  nonEditableText: {
    textAlign: 'center',
    color: Colors.secondary700,
    fontFamily: 'Poppins_700Bold'
  }
});
