import React from 'react';
import { StyleSheet, Text } from 'react-native';
import { useRoute } from '@react-navigation/native';
import FloatingButton from '../../components/FloatingButton';
import Fonts from '../../utill/Fonts';
import { ScrollView } from 'react-native-gesture-handler';
import ExerciseCard from './exercise-card/ExerciseCard';

const WorkoutDetailsScreen: React.FC = () => {
  const route: any = useRoute();

  const addExerciseButtonText: string = 'ADD EXERCISE';
  return (
    <>
      <ScrollView style={styles.container}>
        <Text style={styles.title}>{route.params?.data.workoutName}</Text>
        {route.params?.data.exercises.map((exercise: any, index: number) => (
          <ExerciseCard key={index} exercise={exercise} />
        ))}
      </ScrollView>
      <FloatingButton
        text={addExerciseButtonText}
        onPress={() => console.log('Add exercise')}
      ></FloatingButton>
    </>
  );
};

export default WorkoutDetailsScreen;

const styles = StyleSheet.create({
  container: {
    padding: 12
  },
  text: {
    fontSize: 24,
    fontFamily: Fonts.medium500
  },
  title: {
    textAlign: 'center',
    color: '#000000',
    fontFamily: 'Poppins_700Bold',
    fontSize: 34
  }
});
