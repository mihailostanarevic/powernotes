import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';

import GlobalStyles from '../../../utill/Styles';
import WorkoutCard from './workout-card/WorkoutCard';

const PreviousWorkouts: React.FC = () => {
  return (
    <View style={styles.container}>
      <Text style={GlobalStyles.header}>Previous Workouts</Text>
      <ScrollView>
        <WorkoutCard />
        <WorkoutCard />
        <WorkoutCard />
        <WorkoutCard />
        <WorkoutCard />
        <WorkoutCard />
      </ScrollView>
    </View>
  );
};

export default PreviousWorkouts;

const styles = StyleSheet.create({
  container: {
    marginTop: 24
  }
});
