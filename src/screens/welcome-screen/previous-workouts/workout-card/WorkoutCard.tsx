import React from 'react';
import { Pressable } from 'react-native';
import Card from '../../../../components/Card';
import WorkoutCardHeader from './WorkoutCardHeader';
import WorkoutCardBody from './WorkoutCardBody';

const WorkoutCard: React.FC = () => {
  return (
    <Pressable>
      <Card>
        <WorkoutCardHeader />
        <WorkoutCardBody />
      </Card>
    </Pressable>
  );
};

export default WorkoutCard;
