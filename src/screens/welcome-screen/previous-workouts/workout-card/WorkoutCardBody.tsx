import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';
import GlobalStyles from '../../../../utill/Styles';
import Colors from '../../../../utill/Colors';

const WorkoutCardBody: React.FC = () => {
  return (
    <View style={styles.bodyContainer}>
      <View style={styles.bodyPartsContainer}>
        <Text style={GlobalStyles.subText}>TRAINED</Text>
        <View style={styles.imageContainer}>
          <Image
            resizeMode="contain"
            style={styles.image}
            source={require('../../../../../assets/images/front.png')}
          />
          <Image
            resizeMode="contain"
            style={styles.image}
            source={require('../../../../../assets/images/back.png')}
          />
        </View>
      </View>
      <View style={styles.statsContainer}>
        <View style={styles.statsRowContainer}>
          <View>
            <Text style={GlobalStyles.subText}>EXERCISES</Text>
            <Text style={styles.bold}>5</Text>
          </View>
          <View style={styles.reverseStats}>
            <Text style={GlobalStyles.subText}>TIME</Text>
            <Text style={styles.bold}>1:33:24</Text>
          </View>
        </View>
        <View style={styles.statsRowContainer}>
          <View>
            <Text style={GlobalStyles.subText}>VOLUME</Text>
            <Text style={styles.bold}>990kg</Text>
          </View>
          <View style={styles.reverseStats}>
            <Text style={GlobalStyles.subText}>SETS</Text>
            <Text style={styles.bold}>21</Text>
          </View>
        </View>
      </View>
    </View>
  );
};

export default WorkoutCardBody;

const styles = StyleSheet.create({
  bodyContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 8,
    borderTopWidth: 0.5,
    borderTopColor: Colors.secondary700,
    paddingTop: 8
  },
  bodyPartsContainer: {
    flex: 1,
    alignItems: 'center'
  },
  statsContainer: {
    flex: 1,
    justifyContent: 'space-between',
    padding: 4,
    marginTop: 6
  },
  statsRowContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  reverseStats: {
    alignItems: 'flex-end'
  },
  bold: {
    fontFamily: 'Poppins_700Bold'
  },
  image: {
    height: 100
  },
  imageContainer: {
    flexDirection: 'row'
  }
});
