import React from 'react';
import Ionicons from '@expo/vector-icons/Ionicons';
import { StyleSheet, View, Text } from 'react-native';
import GlobalStyles from '../../../../utill/Styles';
import Colors from '../../../../utill/Colors';

const WorkoutCardHeader: React.FC = () => {
  return (
    <View>
      <View style={styles.cardHeader}>
        <Text style={GlobalStyles.subText}>Tuesday</Text>
        <View style={styles.iconsContainer}>
          <Ionicons
            name="copy"
            style={styles.copyIcon}
            size={24}
            color={Colors.primary500}
          />
          <Ionicons name="share-social" size={24} color={Colors.primary500} />
        </View>
      </View>
      <View style={styles.headerContainer}>
        <Text style={GlobalStyles.header}>Push Day</Text>
      </View>
      <View>
        <Text style={GlobalStyles.subText}>This was an easy day</Text>
      </View>
    </View>
  );
};

export default WorkoutCardHeader;

const styles = StyleSheet.create({
  cardHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  iconsContainer: {
    flexDirection: 'row'
  },
  copyIcon: {
    marginRight: 4
  },
  headerContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 4
  }
});
