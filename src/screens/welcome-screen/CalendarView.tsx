import React from 'react';
import Calendar from '../../components/Calendar';

const CalendarView: React.FC = () => {
  const dates = [new Date('2023-08-28'), new Date('2023-09-28')];

  const onDateSelect = (date: Date) => {
    console.log(date);
  };

  return <Calendar selectedDates={dates} datePressed={onDateSelect} />;
};

export default CalendarView;
