import React from 'react';
import { StyleSheet, View } from 'react-native';
import CalendarView from './CalendarView';
import PreviousWorkouts from './previous-workouts/PreviousWorkouts';
import FloatingButton from '../../components/FloatingButton';

const WelcomeScreen: React.FC = () => {
  const startWorkout = () => {
    console.log('starting workout...');
  };

  return (
    <>
      <View style={styles.container}>
        <CalendarView />
        <PreviousWorkouts />
      </View>
      <FloatingButton onPress={startWorkout} text={floatingButtonText} />
    </>
  );
};

export default WelcomeScreen;

const styles = StyleSheet.create({
  container: {
    padding: 8
  }
});

const floatingButtonText = 'START WORKOUT';
