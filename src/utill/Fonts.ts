const Fonts = {
  medium500: 'Poppins_500Medium',
  bold700: 'Poppins_700Bold'
};

export default Fonts;
