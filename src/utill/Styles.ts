import Colors from './Colors';
import Fonts from './Fonts';

const GlobalStyles = {
  header: {
    fontSize: 16,
    fontFamily: Fonts.bold700
  },
  subText: {
    fontSize: 10,
    fontFamily: Fonts.bold700,
    color: Colors.secondary700
  }
};

export default GlobalStyles;
