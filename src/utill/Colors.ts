const Colors = {
  background: '#F4F4F4',
  primary500: '#D21717',
  primary300: '#D45B5B',
  secondary500: '#F9F9F9',
  secondary700: '#8D8D8D'
};

export default Colors;
