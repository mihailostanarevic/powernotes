const workouts = [
  {
    workoutName: 'Push Day',
    exercises: [
      {
        name: 'Tricep Dips',
        sets: 3,
        weights: [0, 0, 0]
      },
      {
        name: 'Skull Crushers',
        sets: 3,
        weights: [50, 55, 60]
      },
      {
        name: 'Bench Press',
        sets: 4,
        weights: [225, 245, 265, 275]
      },
      {
        name: 'Dumbbell Flyes',
        sets: 3,
        weights: [40, 45, 50]
      },
      {
        name: 'Push-Ups',
        sets: 3,
        weights: [0, 0, 0]
      },
      {
        name: 'Cable Crossover',
        sets: 4,
        weights: [60, 70, 80, 90]
      }
    ],
    note: 'This was an easy day',
    dayOfTheWeek: 'Tuesday',
    duration: '1:33:28'
  },
  {
    workoutName: 'Pull Day',
    exercises: [
      {
        name: 'Deadlift',
        sets: 4,
        weights: [315, 335, 355, 375]
      },
      {
        name: 'Pull-Ups',
        sets: 3,
        weights: [0, 0, 0]
      },
      {
        name: 'Bent Over Rows',
        sets: 4,
        weights: [135, 155, 175, 185]
      },
      {
        name: 'Lat Pulldowns',
        sets: 3,
        weights: [100, 110, 120]
      },
      {
        name: 'Barbell Curls',
        sets: 3,
        weights: [65, 75, 85]
      },
      {
        name: 'Hammer Curls',
        sets: 3,
        weights: [30, 35, 40]
      }
    ],
    note: 'This was an easy day',
    dayOfTheWeek: 'Monday',
    duration: '1:33:27'
  },
  {
    workoutName: 'Push Day',
    exercises: [
      {
        name: 'Tricep Dips',
        sets: 3,
        weights: [0, 0, 0]
      },
      {
        name: 'Skull Crushers',
        sets: 3,
        weights: [50, 55, 60]
      },
      {
        name: 'Bench Press',
        sets: 4,
        weights: [225, 245, 265, 275]
      },
      {
        name: 'Dumbbell Flyes',
        sets: 3,
        weights: [40, 45, 50]
      },
      {
        name: 'Push-Ups',
        sets: 3,
        weights: [0, 0, 0]
      },
      {
        name: 'Cable Crossover',
        sets: 4,
        weights: [60, 70, 80, 90]
      }
    ],
    note: 'This was an easy day',
    dayOfTheWeek: 'Sunday',
    duration: '1:33:27'
  },
  {
    workoutName: 'Pull Day',
    exercises: [
      {
        name: 'Deadlift',
        sets: 4,
        weights: [315, 335, 355, 375]
      },
      {
        name: 'Pull-Ups',
        sets: 3,
        weights: [0, 0, 0]
      },
      {
        name: 'Bent Over Rows',
        sets: 4,
        weights: [135, 155, 175, 185]
      },
      {
        name: 'Lat Pulldowns',
        sets: 3,
        weights: [100, 110, 120]
      },
      {
        name: 'Barbell Curls',
        sets: 3,
        weights: [65, 75, 85]
      },
      {
        name: 'Hammer Curls',
        sets: 3,
        weights: [30, 35, 40]
      }
    ],
    note: 'This was an easy day',
    dayOfTheWeek: 'Saturday',
    duration: '1:33:28'
  }
];

export default workouts;
