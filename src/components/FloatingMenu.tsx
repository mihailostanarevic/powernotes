import { Pressable, StyleSheet, View } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import { FloatingMenuAction } from '../models/FloatingMenuAction';
import Colors from '../utill/Colors';

const FloatingMenu: React.FC<{
  actions: FloatingMenuAction[];
}> = ({ actions }) => {
  return (
    <View style={styles.container}>
      {actions.map((action, index) => (
        <View
          key={index}
          style={[
            styles.menuItem,
            index + 1 !== actions.length && styles.menuItemRightBorder
          ]}
        >
          <Pressable onPress={action.onPress}>
            <Ionicons style={styles.menuIcon} name={action.icon} size={28} />
          </Pressable>
        </View>
      ))}
    </View>
  );
};

export default FloatingMenu;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    flexDirection: 'row',
    alignSelf: 'center',
    bottom: '10%',
    elevation: 3,
    backgroundColor: Colors.secondary500,
    alignItems: 'center',
    padding: 4,
    borderRadius: 12
  },
  menuItem: {
    height: 48,
    width: 52,
    justifyContent: 'center',
    borderColor: Colors.secondary700
  },
  menuItemRightBorder: {
    borderRightWidth: 0.25
  },
  menuIcon: {
    alignSelf: 'center',
    color: Colors.primary500
  }
});
