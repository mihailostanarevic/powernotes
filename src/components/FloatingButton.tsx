import React from 'react';
import { Pressable, StyleSheet, View, Text } from 'react-native';
import Colors from '../utill/Colors';
import GlobalStyles from '../utill/Styles';

type Props = {
  text: string;
  onPress: () => void;
};

const FloatingButton: React.FC<Props> = ({ text, onPress }) => {
  return (
    <Pressable style={styles.container} onPress={onPress}>
      <View style={styles.button}>
        <Text style={[styles.text, GlobalStyles.header]}>{text}</Text>
      </View>
    </Pressable>
  );
};

export default FloatingButton;

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    width: '70%',
    bottom: 0,
    alignSelf: 'center',
    marginBottom: '10%',
    elevation: 3
  },
  button: {
    backgroundColor: Colors.primary500,
    alignItems: 'center',
    paddingHorizontal: 24,
    paddingVertical: 12,
    borderRadius: 12
  },
  text: {
    color: Colors.secondary500
  }
});
