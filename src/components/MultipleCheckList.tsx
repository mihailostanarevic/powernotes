import React from 'react';
import { ScrollView, StyleSheet, Text, View } from 'react-native';
import Fonts from '../utill/Fonts';
import Colors from '../utill/Colors';
import Checkbox from './Checkbox';

const MultipleCheckList: React.FC<{ items: string[] }> = ({ items }) => {
  const itemChecked = (checked: boolean) => {
    console.log(checked);
  };

  return (
    <ScrollView style={styles.mainContainer}>
      {items.map((item, index) => (
        <View
          key={index}
          style={[
            styles.itemContainer,
            index + 1 !== items.length && styles.itemContainerBorder
          ]}
        >
          <Text style={styles.itemText}>{item}</Text>
          <View>
            <Checkbox onChecked={itemChecked} />
          </View>
        </View>
      ))}
    </ScrollView>
  );
};

export default MultipleCheckList;

const styles = StyleSheet.create({
  mainContainer: {
    position: 'absolute',
    padding: 4,
    width: 180,
    backgroundColor: Colors.secondary500,
    zIndex: 2,
    borderWidth: 1,
    borderColor: Colors.secondary700,
    borderRadius: 8,
    right: 20,
    top: 40,
    elevation: 4
  },
  itemContainer: {
    padding: 4,
    borderColor: Colors.secondary700,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  itemContainerBorder: {
    borderBottomWidth: 0.25
  },
  itemText: {
    fontSize: 14,
    fontFamily: Fonts.medium500
  }
});
