import React from 'react';
import { StyleSheet, View } from 'react-native';
import Colors from '../utill/Colors';

const Card: React.FC<React.PropsWithChildren> = ({ children }) => {
  return <View style={styles.card}>{children}</View>;
};

export default Card;

const styles = StyleSheet.create({
  card: {
    marginVertical: 8,
    paddingVertical: 12,
    paddingHorizontal: 8,
    backgroundColor: Colors.secondary500,
    borderRadius: 12,
    borderColor: Colors.secondary700,
    borderWidth: 0.5,
    elevation: 6,
  },
});
