import { KeyboardTypeOptions, TextInput } from 'react-native';
import Colors from '../utill/Colors';

type Props = {
  value?: string;
  type?: KeyboardTypeOptions;
  updateFunction?: (val: any) => void;
  style: {};
  placeholder: string;
  editable: boolean;
};

const Cell: React.FC<Props> = ({
  value,
  type,
  updateFunction,
  style,
  placeholder,
  editable
}) => {
  return (
    <TextInput
      value={value}
      onChangeText={updateFunction}
      style={style}
      placeholder={placeholder}
      placeholderTextColor={Colors.secondary700}
      editable={editable}
      keyboardType={type}
    />
  );
};

export default Cell;
