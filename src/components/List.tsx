import { Text, StyleSheet, FlatList, Pressable } from 'react-native';
import Fonts from '../utill/Fonts';
import Colors from '../utill/Colors';
import { ListItem } from '../models/ListItem';

type Props = {
  items: ListItem<any>[];
  itemSelected?: (item: any) => void;
  itemPressed?: (item: any) => void;
};
const List: React.FC<Props> = ({ items = [], itemSelected, itemPressed }) => {
  return (
    <FlatList
      data={items}
      style={styles.listContainer}
      keyExtractor={(item) => item.id.toString()}
      renderItem={(listItem) => (
        <Pressable
          android_ripple={{ color: Colors.primary300 }}
          style={[
            styles.listItemContainer,
            listItem.item.selected && styles.selectedItemContainer
          ]}
          onLongPress={itemSelected?.bind(null, listItem.item.data)}
          onPress={itemPressed?.bind(null, listItem.item.data)}
        >
          <Text
            style={[
              styles.listItemText,
              listItem.item.selected && styles.selectedItemText
            ]}
          >
            {listItem.item.name}
          </Text>
        </Pressable>
      )}
    ></FlatList>
  );
};

export default List;

const styles = StyleSheet.create({
  listContainer: {
    padding: 8
  },
  listItemContainer: {
    paddingHorizontal: 8,
    paddingVertical: 4,
    borderBottomWidth: 0.25,
    borderRadius: 8
  },
  listItemText: {
    fontSize: 16,
    fontFamily: Fonts.medium500
  },
  selectedItemContainer: {
    backgroundColor: Colors.primary300
  },
  selectedItemText: {
    color: Colors.secondary500
  }
});
