import React, { useState } from 'react';
import { Pressable, StyleSheet, View } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import Colors from '../utill/Colors';

type Props = {
  value?: boolean;
  onChecked: (value: boolean) => void;
};
const Checkbox: React.FC<Props> = ({ value = false, onChecked }) => {
  const [checked, setChecked] = useState(value);

  const checkboxPressed = () => {
    setChecked((value) => !value);
    if (onChecked) {
      onChecked(!value);
    }
  };

  return (
    <Pressable onPress={checkboxPressed}>
      <View style={styles.container}>
        {checked && (
          <Ionicons name="checkmark" size={20} style={styles.checkIcon} />
        )}
      </View>
    </Pressable>
  );
};

export default Checkbox;

const styles = StyleSheet.create({
  container: {
    borderWidth: 1,
    paddingHorizontal: 2,
    borderColor: Colors.primary500,
    width: 26,
    height: 24
  },
  checkIcon: {
    color: Colors.primary500
  }
});
