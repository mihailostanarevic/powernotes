import React, { useState } from 'react';
import { Pressable, StyleSheet, Text, View } from 'react-native';
import Ionicons from '@expo/vector-icons/Ionicons';
import Card from './Card';
import Fonts from '../utill/Fonts';
import Colors from '../utill/Colors';

const compareDates = (currentDate: Date, startDate: Date) => {
  if (currentDate.getFullYear() < startDate.getFullYear()) {
    return true;
  } else if (currentDate.getFullYear() > startDate.getFullYear()) {
    return false;
  }
  if (currentDate.getMonth() <= startDate.getMonth()) {
    return true;
  }
  return false;
};

const areDatesTheSame = (date: Date, toCompare: Date) => {
  return (
    toCompare.getFullYear() === date.getFullYear() &&
    toCompare.getMonth() === date.getMonth() &&
    toCompare.getDate() === date.getDate()
  );
};

const Calendar: React.FC<{
  selectedDates: Date[];
  datePressed: any;
}> = ({ selectedDates, datePressed }) => {
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [weekMode, setWeekMode] = useState(false);

  const startDate = new Date(selectedDate);

  // If we are in month mode then the start date is the first day of the month
  if (!weekMode) {
    startDate.setDate(1);
  }

  // Get the first day of the week (Monday) of the start date regardless if it's in the previous month or not
  if (startDate.getDay() > 1) {
    startDate.setDate(startDate.getDate() - startDate.getDay() + 1);
  } else if (startDate.getDay() === 0) {
    startDate.setDate(startDate.getDate() - 6);
  }

  // Calculate dates to show, if we are in week mode we only show one week
  let dates: Date[][] = [];
  while (compareDates(startDate, selectedDate)) {
    const week: Date[] = [];
    for (let index = 0; index < 7; index++) {
      const date = new Date(startDate);
      date.setDate(startDate.getDate() + index);
      week.push(date);
    }
    dates.push(week);
    startDate.setDate(startDate.getDate() + 7);
    if (weekMode) {
      break;
    }
  }

  // Flip the matrix around for drawing purposes
  dates = dates[0].map((_, colIndex) => dates.map((row) => row[colIndex]));

  const calendarViewChanged = (forward: boolean) => {
    const newSelectedDate = new Date(selectedDate);
    if (weekMode) {
      const advance = forward ? 7 : -7;
      newSelectedDate.setDate(newSelectedDate.getDate() + advance);
    } else {
      const advance = forward ? 1 : -1;
      newSelectedDate.setMonth(newSelectedDate.getMonth() + advance);
    }
    setSelectedDate(newSelectedDate);
  };

  const changeMode = () => {
    setWeekMode((mode) => !mode);
  };

  const dateSelected = (date: Date) => {
    return selectedDates?.filter((d) => areDatesTheSame(d, date)).length > 0;
  };

  return (
    <View>
      <View style={styles.calendarHeader}>
        <Ionicons
          name="chevron-back"
          size={20}
          style={styles.monthsIcon}
          onPress={calendarViewChanged.bind(null, false)}
        />
        <View style={styles.monthContainer}>
          <Text style={styles.headerText}>
            {months[selectedDate.getMonth()]}
            {selectedDate.getFullYear()}
          </Text>
          <Ionicons
            name={weekMode ? 'caret-down' : 'caret-up'}
            size={20}
            onPress={changeMode}
            style={styles.weekIcon}
          />
        </View>
        <Ionicons
          name="chevron-forward"
          size={20}
          style={styles.monthsIcon}
          onPress={calendarViewChanged.bind(null, true)}
        />
      </View>

      <Card>
        <View style={styles.mainContainer}>
          {dates.map((weekDay, index) => (
            <View key={index} style={styles.calendarColumn}>
              <Text style={styles.daysOfTheWeek}>{weekDays[index]}</Text>
              {weekDay.map((day) => (
                <Pressable
                  key={day.toISOString()}
                  onPress={datePressed?.bind(null, day)}
                >
                  <Text
                    style={[
                      styles.day,
                      areDatesTheSame(day, new Date()) && styles.currentDate,
                      selectedDate.getMonth() !== day.getMonth() &&
                        styles.alternateDate,
                      dateSelected(day) && styles.selectedDate,
                    ]}
                  >
                    {day.getDate()}
                  </Text>
                </Pressable>
              ))}
            </View>
          ))}
        </View>
      </Card>
    </View>
  );
};

export default Calendar;

const styles = StyleSheet.create({
  calendarHeader: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  monthContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  monthsIcon: {
    marginHorizontal: 8,
    paddingHorizontal: 24,
    paddingVertical: 4,
  },
  mainContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  weekIcon: {
    paddingHorizontal: 8,
    paddingVertical: 2,
    position: 'absolute',
    right: -30,
    top: -1,
  },
  calendarColumn: {
    flex: 1,
  },
  headerText: {
    fontSize: 14,
    alignSelf: 'center',
    fontFamily: Fonts.bold700,
  },
  daysOfTheWeek: {
    alignSelf: 'center',
    marginBottom: 4,
  },
  day: {
    fontSize: 16,
    fontFamily: Fonts.medium500,
    marginVertical: 2,
    width: '100%',
    paddingVertical: 2,
    borderRadius: 50,
    textAlign: 'center',
    alignSelf: 'center',
  },
  currentDate: {
    borderWidth: 0.25,
    backgroundColor: Colors.secondary700,
    color: Colors.secondary500,
  },
  alternateDate: {
    color: Colors.secondary700,
  },
  selectedDate: {
    color: Colors.primary500,
  },
});

const months = [
  'January',
  'February',
  'March',
  'April',
  'May',
  'June',
  'July',
  'August',
  'September',
  'October',
  'November',
  'December',
];

const weekDays = ['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
