import 'react-native-gesture-handler';

import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import {
  useFonts,
  Poppins_500Medium,
  Poppins_700Bold
} from '@expo-google-fonts/poppins';
import Colors from './src/utill/Colors';
import WelcomeScreen from './src/screens/welcome-screen/WelcomeScreen';
import { useEffect, useState } from 'react';
import Exercises from './src/screens/exercise-screen/Exercises';
import Ionicons from '@expo/vector-icons/Ionicons';
import { StyleSheet, Text } from 'react-native';
import Fonts from './src/utill/Fonts';
import WorkoutDetailsScreen from './src/screens/workout-screen/WorkoutDetailsScreen';
import React from 'react';

export default function App() {
  const [appLoaded, setAppLoaded] = useState(false);

  const [fontsAreLoaded] = useFonts({
    Poppins_500Medium,
    Poppins_700Bold
  });

  useEffect(() => {
    if (fontsAreLoaded) setAppLoaded(true);
  }, [fontsAreLoaded]);

  if (!appLoaded) {
    return null;
  }

  const Stack = createStackNavigator();
  return (
    <>
      <StatusBar></StatusBar>
      <NavigationContainer>
        <Stack.Navigator
          screenOptions={{
            cardStyle: {
              backgroundColor: Colors.background
            },
            headerStyle: {
              backgroundColor: Colors.secondary500
            },
            headerTintColor: Colors.primary500,
            headerLeft: () => (
              <Ionicons name="barbell" style={styles.headerIcons} size={48} />
            ),
            headerRight: () => (
              <Ionicons name="cog" style={styles.headerIcons} size={32} />
            ),
            headerTitle: () => {
              return <Text style={styles.title}>Power Notes</Text>;
            }
          }}
        >
          <Stack.Screen name="Home" component={WelcomeScreen}></Stack.Screen>
          <Stack.Screen name="Exercises" component={Exercises}></Stack.Screen>
          <Stack.Screen
            name="Workout"
            component={WorkoutDetailsScreen}
          ></Stack.Screen>
        </Stack.Navigator>
      </NavigationContainer>
    </>
  );
}

const styles = StyleSheet.create({
  headerIcons: {
    color: Colors.primary500,
    marginHorizontal: 8
  },
  title: {
    fontFamily: Fonts.bold700,
    color: Colors.primary500,
    fontSize: 18
  }
});
